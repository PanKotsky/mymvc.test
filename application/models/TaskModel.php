<?php

/**
 * Created by PhpStorm.
 * User: PanKockyj
 * Date: 10.09.2017
 * Time: 0:10
 */
class TaskModel extends Model
{

    public static function getTask($tid = null, $order = null)
    {
        $pdo = parent::getDb();
        if ($tid && !is_null($tid)) {
            $query = $pdo->prepare("SELECT * FROM `task` LEFT JOIN `user` ON `task`.`executer` = `user`.`id` WHERE `user`.`taskId` = :tid");
            $query->execute(['tid' => $tid]);
        } else {
            $prequery = "SELECT * FROM `task` LEFT JOIN `user` ON `task`.`executer` = `user`.`id`";
            if ($order) {
                $prequery .= "ORDER BY $order";
            }
            $query = $pdo->query($prequery);

        }
        while ($row = $query->fetch()) {
            $result[] = $row;
        }

        return $result;
    }


    public static function saveTask($task, $userId, $image = null){
        $pdo = parent::getDb();
        $query = $pdo->prepare("INSERT INTO `task` SET title = :title, description = :description, executer = :userId, img = :image");
        $query->execute(['title' => $task['title'], 'description' => $task['description'], 'userId' => $userId, 'image' => $image]);
        return true;
    }

    public static function editTask($data)
    {
        $pdo = parent::getDb();
        $prequery = "UPDATE `task` SET ";
        if (isset($data['description'])) {
            $prequery .= "`description` = :description ,";
            $data['status'] = 0;
        }
        if (isset($data['status']) && !is_null($data['status'])) {
            $prequery .= " `status` = :status";
        }
        $prequery .= " WHERE `taskId` = ".$data['id'];

        $query = $pdo->prepare($prequery);
        $query->execute(['description' => $data['description'], 'status' => $data['status']]);

        return true;
    }

}