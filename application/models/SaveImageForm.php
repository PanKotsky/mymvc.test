<?php

class SaveImageForm
{
    /**
     * Save Image
     * @param $image
     * @return bool|string
     */
    public static function saveImage($image){
        if($image){
            $pathToImage = 'web/images/';
            if(!is_dir($pathToImage)){
                mkdir($pathToImage, 0755);
            }

            $type = exif_imagetype($image['file']['tmp_name']);
            switch($type) {
                case 1:
                    $file = imagecreatefromgif($image['file']['tmp_name']);
                    $ext = 'gif';
                    break;
                case 2:
                    $file = imagecreatefromjpeg($image['file']['tmp_name']);
                    $ext = 'jpg';
                    break;
                case 3:
                    $file = imagecreatefrompng($image['file']['tmp_name']);
                    $ext = 'png';
                    break;
            }

            $size = getimagesize($image['file']['tmp_name']);

            if($size[0] > 320 && $size[1] > 240) {
                $file = self::resize($file);
            }

            $nameImage = md5(rand(0, getrandmax()));
            $nameImage = $nameImage.'.'.$ext;
            $fullPath = $pathToImage.$nameImage;
            if(imagejpeg($file, $fullPath)){
                return $nameImage;
            }
            else {
                return 'Something wrong';
            }
        }
        return false;
    }

    /**
     * Resize image
     * @param $file
     * @return resource
     */
    public function resize($file){

        $correlation = imagesy($file)/imagesx($file);
        $nwidth = 320;
        $nheight = 240;
        /*для збереження пропорцій зображення*/
        /* якщо у зображення стандартні пропорції 4х3 */
        if($correlation == 0.75){
            $nwidth = 320;
            $nheight = 240;
        }
        /* якщо зображення широкоформатне */
        elseif($correlation <= 0.75){
            $nwidth = 320;
            $nheight = $nwidth*$correlation;
        }
        /* якщо зображення має висоту більшу ніж ширину*/
        elseif ($correlation >= 0.75){
            $nheight = 240;
            $nwidth = $nheight/$correlation;
        }

        $im1=imagecreatetruecolor(intval($nwidth),intval($nheight));
        imagecopyresampled($im1,$file,0,0,0,0,intval($nwidth),intval($nheight),imagesx($file),imagesy($file));
        return $im1;
    }
}