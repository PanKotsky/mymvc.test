<?php

class UserModel extends Model
{

    public static function getUserById(){
        $pdo = parent::getDb();
        $result = $pdo->query("SELECT * FROM `user` WHERE `id` = 1");

        return $result;
    }

    public static function userLogin($login, $password){
        $password = strip_tags(trim($password));
        $pdo = parent::getDb();

        $query = $pdo->prepare("SELECT * FROM `user` INNER JOIN `role` ON `user`.`role` = `role`.`roleId` WHERE `username` = :login");
        $query->execute(['login' => $login]);

        $result = $query->fetch();

        if(!$result){
            return false;
        }

        if($result){
            $identity = new UserIdentity($result);
            $identity->login($password);
        }

        return $result;
    }

    public static function getUserByEmail($email){
        $pdo = parent::getDb();

        $query = $pdo->prepare("SELECT * FROM `user` WHERE `email` = :email");
        $query->execute(['email' => $email]);

        $result = $query->fetch();

        return $result;
    }

    /*
     * Метод використовується лише при створенні таска з зазначенням пошти, якої немає у жодного користувача,
     * відповідно щоб не створювати додаткового функціоналу реєстрації (про який не йдеться в ТЗ) створюється запис в таблиці user
     * з дефолтними прааметрами, а саме логін і пошта як вказана при створені таску пошті і дефолтний пароль - 111111
     *
     * За потреби - функціонал змінюється і доповнюється
     */
    public static function createUser($email){
        $pdo = parent::getDb();

        $query = $pdo->prepare("INSERT INTO `user` SET `email` = :email, `username` = :username, `password` = '111111', `role` = 2");
        $query->execute(['email' => $email, 'username' => $email]);

        $result = $pdo->lastInsertId();

        return $result;
    }

}