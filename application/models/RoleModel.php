<?php

/**
 * Created by PhpStorm.
 * User: PanKockyj
 * Date: 10.09.2017
 * Time: 11:18
 */
class RoleModel extends Model
{
    public $role;

    public static function getRole($uid){
        $pdo = parent::getDb();

        $query = $pdo->prepare("SELECT * FROM `user` LEFT JOIN `role` ON `user`.role = `role`.`roleId` WHERE user.role = :uid");
        $query->execute(['uid' => $uid]);

        return $query;
    }

}