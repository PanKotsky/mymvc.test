<div class="container col-md-6 col-md-offset-5">
    <div class="row background-row">
        <div class="center-block ">
            <p class="login">Введіть Ваш логін та пароль</p>
            <div class="center col-sm-4">
                <form class="form-horizontal" action="login/login" method="post">
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Логін</label>
                        <div class="controls">
                            <input type="text" name="username" id="username" placeholder="Логін">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputPassword">Пароль</label>
                        <div class="controls">
                            <input type="password" name="password" id="password" placeholder="Пароль">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <button type="submit" class="btn">Залогінитись</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="center col-sm-4">
                <p class="alert alert-danger warning_login"><?= $_SESSION['warning_login'] ?></p>
                <?php unset($_SESSION['warning_login']); ?>
            </div>
        </div>
    </div>
</div>

<script src="/web/js/login.js"></script>
