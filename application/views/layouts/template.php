<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Тестове завдання</title>
    <link rel="stylesheet" type="text/css" href="/web/css/style.css" />
    <link rel="stylesheet" type="text/css" href="/web/assets/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/web/assets/datatables/css/jquery.dataTables.min.css" />
    <script src="/web/assets/jquery/jquery-3.2.1.min.js"></script>
    <script src="/web/assets/jeditable/jquery.jeditable.js"></script>
    <script src="/web/assets/datatables/js/jquery.dataTables.min.js"></script>
</head>
<body>
<?php include 'application/views/'.$view; ?>

</body>
</html>