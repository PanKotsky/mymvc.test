<h1>Congratulations</h1>

<a href="/task/create" class="btn btn-info">Створити таск</a>
<?php if(!$_SESSION['login']) { ?>
    <a href="/login/login" class="btn btn-info">Логін</a>
<?php } else { ?>
    <?php if($_SESSION['admin']) { ?>
        <a href="/admin/index" class="btn btn-info">Для адміна</a>
    <?php } ?>
    <a href="/login/logout" class="btn btn-info">Вийти</a>
<?php } ?>

<table id="example" class="display" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Назва</th>
            <th>Задача</th>
            <th>Користувач</th>
            <th>Email</th>
            <th>Зображення</th>
            <th>Статус</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($tasks as $task) : ?>
        <tr>
            <td><?=$task['title']?></td>
            <td><?=$task['description']?></td>
            <td><?=($task['username']) ? $task['username'] : "-"?></td>
            <td><?=($task['email']) ? $task['email'] : "-"?></td>
            <td><img class="group list-group-image" src="/web/images/<?=$task['img']?>" alt="" width="320px"/></td>
            <td><?=($task['status']) ? "Виконано" : "Не виконано"?></td>
        </tr>
    <?php endforeach; ?>
    <tbody>
</table>


<script src="/web/js/siteIndex.js"></script>
