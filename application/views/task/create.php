<div class="container col-md-6 col-md-offset-3">
    <div class="create_task .center-block ">
        <p class="title_create_task">Створення задачі</p>
        <div class="wrapper">
            <form class="form-horizontal" id="task" name="task" action="/task/create" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Назва</label>
                        <div class="col-sm-10">
                            <input class="form-control input-lg" type="text" name="title" id="title" placeholder="Назва таску..." required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Опис</label>
                        <div class="col-sm-10">
                            <textarea class="form-control input-lg" name="description" id="description" cols="30" rows="10" placeholder="Опис таску" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Опис</label>
                        <div class="col-sm-10">
                            <input class="form-control input-lg" type="email" name="email" id="description"  placeholder="Ваш E-mail" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="file" class="col-sm-2 control-label">Додати зображення</label>
                        <div class="col-sm-10">
                            <input type="file" name="file" id="file" accept="image/jpeg,image/png,image/gif">
                            <p class="help-block">Будь ласка, не завантажуйте фото більше 320х240</p>
                            <p class="help-block">інакше вони будуть пропорційно зжаті до зазначеного розміру</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" id="submit_create_tour" class="btn btn-default">Зберегти</button>
                            <a href="/site/index" class="btn btn-default">Повернутись</a>
                            <a href="#" id="preview" class="btn btn-default">Попередній перегляд</a>
                        </div>
                    </div>
                    <?php if(isset($_SESSION['message'])) : ?>
                        <p class="alert alert-success success_msg"><?= $_SESSION['message'] ?></p>
                        <?php unset($_SESSION['message']); ?>
                    <?php endif; ?>
                </div>
                <div id="preview_block">
                    <table id="example" class="display" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th class="title">Назва</th>
                            <th class="description">Задача</th>
                            <th>Користувач</th>
                            <th class="email">Email</th>
                            <th class="image">Зображення</th>
                            <th class="status">Статус</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="/web/js/taskCreate.js"></script>