<table id="example" class="display" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Назва</th>
            <th>Задача</th>
            <th>Користувач</th>
            <th>Email</th>
            <th>Зображення</th>
            <th>Статус</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($tasks as $task) : ?>
        <tr>
            <td><?=$task['title']?></td>
            <td class="editable" data-id="<?= $task['taskId']?>"><?=$task['description']?></td>
            <td><?=($task['username']) ? $task['username'] : "-"?></td>
            <td><?=($task['email']) ? $task['email'] : "-"?></td>
            <td><img class="group list-group-image" src="/web/images/<?=$task['img']?>" alt="" width="320px"/></td>
            <td class="select_editable" data-id="<?= $task['taskId']?>" id="<?= $task['taskId']?>"><?=($task['status']) ? "Виконано" : "Не виконано"?></td>
        </tr>
        <?php endforeach; ?>
    <tbody>
</table>

<a href="/site/index" class="btn btn-success">Повернутись</a>


<script src="/web/js/admin.js"></script>
<script type="text/javascript">

    function editTable(editable) {
        setTimeout(function () {
            editable = editable ? editable : ".editable";
            $(editable).each(function() {
                $(this).editable(function(value, id, settings) {
                    var taskId = $(this).data('id');
                    $.ajax({
                       url: '/admin/edit',
                       data: {description: value, id: taskId},
                       type: "POST",
                       success: function () {
                       }
                    });
                    return(value);
                    }, {
                        type    : 'textarea',
                        submit  : 'OK',
                        cancel: 'Отмена',
                        data: $(this).text(),
                        tooltip: "Клікніть, щоб відредагувати цей текст"
                    }
                );
            });

            $('.select_editable').editable(function(value, id, settings) {
                var taskId = $(this).data('id');
                $.ajax({
                    url: '/admin/edit',
                    data: {status: value, id: taskId},
                    type: "POST",
                    success: function () {
                    }
                });

                return(id.data[value]);
            }, {
                type    : 'select',
                submit  : 'OK',
                cancel: 'Отмена',
                data: {'0':'Не виконано','1':'Виконано'},
                tooltip: "Клікніть, щоб змінити статус"
            });
        }, 500);
    }

    window.addEventListener("load",function(){
        editTable();
    });

</script>