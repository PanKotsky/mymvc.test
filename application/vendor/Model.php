<?php

class Model
{

    /**
     * Database query
     * @param string $sql
     * @return bool|mixed
     */
    public function getQueryDb($sql){
        $result =  Db::query($sql);
        return $result;
    }

    public function getDb(){
        $result =  Db::getDb();
        return $result;
    }

}