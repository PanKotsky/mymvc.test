<?php
$display_errors = 1;
$display_startup_errors = 1;
$error_reporting = E_ALL;

class UserIdentity
{
    protected $user;
    protected $status;
    protected $role;
    protected $session_id;

    public function __construct($user){
        $this->setUser($user);
        $this->setSessionId($_COOKIE['session_id']);
        if($this->user){
            $this->setRole($this->user['roleName']);
        }
    }

    public function login($password){

        $security = new Security();
        $_user = $this->getUser();
        $validatePassword = $security->validatePassword($password, $_user['password']);

        if(!$validatePassword){
            return false;  // тут має бути ексепшн - паролі не співпали
        }

        $_session_id = $this->getSessionId();
        if($_user && !$_session_id){

            $session = $security->createSession($_user);
            if(!$session){
                return false;  // тут має бути ексепшн - сесія не створилась
            }
            $_SESSION['role'] = $this->getRole();
            $_COOKIE['session_id'] = $session['id'];
            return true;
        }
        return false;
    }

    public function logout() {
        unset($_SESSION);
        return true;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param mixed $session_id
     */
    public function setSessionId($session_id)
    {
        $this->session_id = $session_id;
    }

    /**
     * @return mixed
     */
    public function getSessionId()
    {
        return $this->session_id;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

}