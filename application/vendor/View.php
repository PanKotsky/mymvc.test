<?php

class View
{
    public $controller;

    public $layouts = 'layouts';

    public function __construct($controller) {
        $this->controller = $controller;
    }

    /**
     * Default template
     * @var
     */
    public $template_view;

    /**
     * Renders a view and applies layout if available.
     * @param $contentView
     * @param $templateView
     * @param mixed $data
     */
    function render($contentView, $templateView, $content = null){
        /*
        if(is_array($data)) {
            extract($data);
        }
        */
        $contentView = $contentView.'.php';
        $templateView = $templateView.'.php';

        if($content){
            foreach ($content as $key => $data){
                $$key = $data;

            }
        }

        $view = $this->getContent($this->controller, $contentView);

        include 'application/views/layouts/'.$templateView;
    }

    /**
     * Get path to template
     * @param $templateView
     * @return string
     */
    public function getViewPath($templateView){
        return 'application/views/'.$this->controller.'/'.$templateView;
    }

    /**
     * Get path to view
     * @param $controller
     * @param $contentView
     * @return string
     */
    public function getContent($controller, $contentView){
        return $controller.'/'.$contentView;
    }

}