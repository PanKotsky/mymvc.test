<?php

class Controller {

    public $model;
    public $view;
    public $controller;

    function __construct($controller)
    {
        $this->view = new View($controller);
        $this->controller = $controller;
    }

    /**
     * Get name current controller. Used in route to view.
     * @return mixed
     */

    public function getController(){
        return $this->controller;
    }

    /**
     * Дуже тупо, але вже 3 ночі, креатив занепав. Я перероблю, абісцяю.
     * @param $path
     */
    public function redirect($path){
        header('Location: '.$path);
    }

    /**
     * Temporary method
     */
    public function ErrorPage404() {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }
}