<?php

class Route
{

    public static function start() {
        /**
         * default controller and action
         */
        $controllerTitleName = 'Site';
        $actionName = 'Index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        /**
         * get name controller
         */
        if ( !empty($routes[1]) )
        {
            $controllerTitleName = ucfirst($routes[1]);
        }

        /**
         * get name action
         */
        if ( !empty($routes[2]) )
        {
            $actionName = ucfirst($routes[2]);
        }

        /**
         * add modifier
         */
        $modelName = $controllerTitleName.'Model';
        $controllerName = $controllerTitleName.'Controller';
        $actionName = 'action'.$actionName;

        // add file with class of model (it's optional)

        $modelFile = $modelName.'.php';
        $modelPath = "application/models/".$modelFile;

        /*if(file_exists($modelPath))
        {
            include "application/models/".$modelFile;
        }
        else{*/

        /**
         * include all models
         */
            $files = glob("application/models/*");
            foreach ($files as $file){
                if(!is_file($file)) continue;
                require_once($file);
            }
        /*}*/

        // подцепляем файл с классом контроллера
        /**
         * add file with class of controller
         * if file exist - add
         * else - Exception
         */
        $controllerFile = $controllerName.'.php';
        $controllerPath = "application/controllers/".$controllerFile;
        if(file_exists($controllerPath))
        {
            include "application/controllers/".$controllerFile;
        }
        else
        {
            /**
             * ToDo add Exception
             */
            Route::ErrorPage404();
        }

        /**
         * create controller
         */
        $controller = new $controllerName(lcfirst($controllerTitleName));
        $action = $actionName;

        /**
         * call action in controller
         * if method exist - call
         * else - Exception
         */

        if(method_exists($controller, $action))
        {
            $controller->$action();
        }
        else
        {
            /**
             * ToDo add Exception
             */
            Route::ErrorPage404();
        }

    }

    /**
     * Temporary method
     */
    public function ErrorPage404() {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }
}