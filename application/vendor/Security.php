<?php

class Security
{
    protected $session;
    protected $user;
    protected $hash;
    protected $statusSession;


    const SESSION_DISABLED = 0;
    const SESSION_NONE = 1;
    const SESSION_ACTIVE = 2;

    public function __construct() {

    }

    public function createSession($user){
        if($this->getStatusSession() !== self::SESSION_DISABLED){
           // session_start();
            $_SESSION['login'] = true;
            $_SESSION['user'] = $user['login'];
            $_SESSION['password'] = $user['password'];
            return true;
        }
        return false;
    }

    public function validatePassword($password, $savedUserPassword){
        if(!password_verify(strval($password), $savedUserPassword)){
            return false;
        }
        return true;
    }

    protected function getStatusSession(){
        return session_status();
    }

    private function encodeSession($elem1, $elem2){
        return base64_encode($elem1.$elem2);
    }

}