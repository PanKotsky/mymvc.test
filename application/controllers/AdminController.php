<?php

class AdminController extends Controller
{

    public function actionIndex(){
        if($_SESSION['admin']) {
            $allTask = TaskModel::getTask();
            $this->view->render('index', 'template', ['tasks' => $allTask] );
        }
        else{
            $this->ErrorPage404();
        }
    }

    public function actionEdit(){
        $data = null;
        $task = $_POST;

        foreach ($task as $key => $description){
            $data[$key] = $description;
        }

        if(TaskModel::editTask($data)){
            return true;
        }

        return false;
    }

    public function actionStatus(){

    }
}