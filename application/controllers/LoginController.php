<?php

class LoginController extends Controller
{
    public function actionLogin()
    {

        if($_POST['username'] != '' && $_POST['password'] != '')
        {

            unset($_SESSION['warning_login']);
            $login = $_POST['username'];
            $password =$_POST['password'];

            $userData = UserModel::userLogin($login, $password);

            if(!$userData){
                $_SESSION['warning_login'] = "Вибачте, але такого користувача не існує";
                $this->redirect('/login/login');
                exit;
            }

            $this->redirect('/site/index');

        }
        else{
            $_SESSION['warning_login'] = "Ви не ввели логін та пароль";
        }

        $this->view->render('login', 'template');
    }

    public function actionLogout(){
        unset($_SESSION['login']);
        $this->redirect('/site/index');
    }

}