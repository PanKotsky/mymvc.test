<?php

/**
 * Created by PhpStorm.
 * User: PanKockyj
 * Date: 10.09.2017
 * Time: 15:13
 */

class TaskController extends Controller
{

    public function actionCreate(){
        $task = $_POST;
        $image = $_FILES;

        $saveImage = null;

        if($task && !is_null($task)) {
            if($image['file']['tmp_name']) {
                $saveImage = SaveImageForm::saveImage($image);
            }

            $user = UserModel::getUserByEmail($task['email']);
            if(!$user){
                $user['id'] = UserModel::createUser($task['email']);
            }

            TaskModel::saveTask($task, $user['id'], $saveImage);
            $_SESSION['message'] = 'Новий таск увспішно створено';

        }

        $this->view->render('create', 'template');
    }



}