<?php

class Db{

    public $dsn;

    public $username;

    public $password;

    public $charset;

    public $attributes;

    public $pdo;

    /**
     * instance of the class
     */
    protected static $_instance;

    /** get instance of the class
     * @return Db
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Db constructor.
     */
    private function __construct() {;
        $config = null;
        require_once 'configDb.php';
        require(__DIR__ . '/configDb.php');
        $this->username = $config['username'];
        $this->password = $config['password'];
        $this->dsn = $config['dsn'];
        $this->charset = $config['charset'];

        $dsn = $this->dsn.';charset='.$this->charset;
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->pdo = new PDO($dsn, $this->username, $this->password, $opt);

    }

    public function __destruct(){
    }

    private function __clone() {
    }

    private function __wakeup() {
    }

    /**
     * Query to database
     *
     * @param $sql
     * @return bool|mixed
     */
    public static function query($sql) {

        $obj = self::getInstance();

        if(isset($obj->pdo)){
            $stmt = $obj->pdo->query($sql);

            while ($row = $stmt->fetch()) {
                $result[] = $row;
            }


            $obj->pdo = null;
            $stmt = null;
            self::$_instance = null;
            return $result;
        }
        return false;
    }

    public static function getDb(){
        $obj = self::getInstance();
        return $obj->pdo;
    }

}