<?php

/**
 * include basing files of application: model, view, controller and file for routing
 */

require_once 'vendor/Model.php';
require_once 'vendor/View.php';
require_once 'vendor/Controller.php';
require_once 'vendor/Route.php';
require_once 'vendor/UserIdentity.php';
require_once 'vendor/Security.php';
require_once 'db.php';

Route::start();
