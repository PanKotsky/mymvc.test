$(document).ready(function(){
// window.addEventListener("load",function(){
    $('#submit_create_tour').click(function (e) {

        var task = $('form #task').serialize();
        var action = $('form').attr("action");
        var input = $('#task');
        var upload = new FormData;

        // upload.append('img', input.prop('files')[0].files[0]);
        upload.append('img', $('input[id="file"]')[0].files[0]);
        upload.append('task', task);

        console.log(upload);
        $.ajax({
            cache: false,
            processData: false,
            contentType: false,
            type: "POST",
            url: action,
            data: upload,
            success: function() {
            },
            error: function() {
            }
        });
    });


    $('.success_msg').fadeToggle(3000);

    $('#preview').click(function(){
       var form = $('form');
       var title = $('form input[name=title]').val();
       var description = $('form textarea[name=description]').val();
       var email = $('form input[name=email]').val();
       var pattern = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/g;

        var display = $('#preview_block').css('display');
        if(title == '' && description == '' && email == '') {
            alert('Заповніть поля');
            return;
        }
        if(display == 'none'){

            if(!pattern.test(email)){
                alert('Ви ввели невірний формат Email!');
            }


           // $('#preview_title').val(title);
           // $('#preview_description').val(description);
            $("#example").dataTable({
                "bFilter": false,
                "paging": false,
                "lengthMenu": false,

                "aaData":[
                    [title,description,"-",email,"-","Не виконано"],
                ],
                "aoColumnDefs":[{
                    "aTargets": [ "title" ]
                },{
                    "aTargets": [ "description" ]
                },{
                    "aTargets": [ "email" ]
                },/*{
                    "aTargets":[ "image" ]
                    , "sType": "file"
                    , "mRender": function(img, type, full) {
                        return  '<img class="group list-group-image" src="'+img+'" alt="" width="320px"/>';
                    }
                },*/{
                    "aTargets": [ "status" ]
                }
                ]
            });
        }
        else {
            $("#example").dataTable().fnDestroy();
        }


       $('#preview_block').toggle(300);
    });

});