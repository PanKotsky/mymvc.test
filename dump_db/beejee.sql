-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 11 2017 г., 11:26
-- Версия сервера: 5.7.16
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `beejee`
--

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

CREATE TABLE `role` (
  `roleId` int(1) NOT NULL,
  `roleName` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `role`
--

INSERT INTO `role` (`roleId`, `roleName`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `task`
--

CREATE TABLE `task` (
  `taskId` int(11) NOT NULL,
  `title` varchar(300) DEFAULT NULL,
  `description` text,
  `img` varchar(100) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `executer` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `task`
--

INSERT INTO `task` (`taskId`, `title`, `description`, `img`, `status`, `executer`) VALUES
(1, 'Test', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo aliquid sint a voluptatem facere mollitia aut, eveniet velit optio aliquam?', 'NASA_80.jpg', 0, 2),
(2, 'Test2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam voluptates, illum! Maxime nulla aspernatur nam magnam obcaecati harum, numquam vero, quidem sit dolor, fugiat ex laborum eos. Voluptatem, commodi, voluptatibus. Porro fuga ipsa reprehenderit ipsum qui quae et velit animi. Nesciunt repellendus, maiores accusamus porro perferendis alias totam doloribus quidem!', 'NASA_76.jpg', 0, NULL),
(3, 'Test4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio deleniti saepe amet reprehenderit aliquam sit consectetur labore, expedita molestias vitae sequi praesentium architecto similique, quod temporibus aperiam alias repudiandae corporis mollitia error itaque laborum tempora, natus eos! Officiis voluptatum totam laborum asperiores unde nemo facilis praesentium, quos officia magni veritatis odit et incidunt, repudiandae modi in nostrum facere inventore at ut voluptates! Nostrum quas odit ratione accusantium amet, hic porro?', '58589b2f5ef1c0bbfdcd09c6fb0b47b7.jpg', 0, NULL),
(4, 'Test4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio deleniti saepe amet reprehenderit aliquam sit consectetur labore, expedita molestias vitae sequi praesentium architecto similique, quod temporibus aperiam alias repudiandae corporis mollitia error itaque laborum tempora, natus eos! Officiis voluptatum totam laborum asperiores unde nemo facilis praesentium, quos officia magni veritatis odit et incidunt, repudiandae modi in nostrum facere inventore at ut voluptates! Nostrum quas odit ratione accusantium amet, hic porro?', '14b5caec7ac6a9609e748d56a17c174b.jpg', 0, NULL),
(5, 'Test4', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio deleniti saepe amet reprehenderit aliquam sit consectetur labore, expedita molestias vitae sequi praesentium architecto similique, quod temporibus aperiam alias repudiandae corporis mollitia error itaque laborum tempora, natus eos! Officiis voluptatum totam laborum asperiores unde nemo facilis praesentium, quos officia magni veritatis odit et incidunt, repudiandae modi in nostrum facere inventore at ut voluptates! Nostrum quas odit ratione accusantium amet, hic porro?', 'e798a0d5dea21ef441815910d5943598.jpg', 0, NULL),
(6, 'Test7777', 'ewedvfgvdv', '92a795b46873063e419773b4f310fc29.jpg', 0, NULL),
(7, 'Test7777', 'ewedvfgvdv', 'bc7316929fe1545bf0b98d114ee3ecb8.jpg', 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `role`) VALUES
(1, 'Kockyj', 'pan_kockyj@ukr.net', '111111', 1),
(2, 'admin', 'admin@test.com', '123', 1),
(3, 'Samsung', 'samsung@test.com', '111111', 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`roleId`);

--
-- Индексы таблицы `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`taskId`),
  ADD KEY `FK_task_user_idx` (`executer`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_user_role_idx` (`role`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `role`
--
ALTER TABLE `role`
  MODIFY `roleId` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `task`
--
ALTER TABLE `task`
  MODIFY `taskId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `FK_task_user` FOREIGN KEY (`executer`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_user_role` FOREIGN KEY (`role`) REFERENCES `role` (`roleId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
